<%-- 
    Document   : listar
    Created on : 08-10-2021, 17:48:31
    Author     : paula
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.club.entity.Inscripcion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Inscripcion> lista = (List<Inscripcion>) request.getAttribute("lista");

    Iterator<Inscripcion> itlista = lista.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
         <br>
        <form class="form-horizontal" action="InscripcionController" method="GET">
            <h1 style="background-color:Orange;">Integrantes</h1>
             <br>
            <table border="1">
                <thead>
                <th>rut</th>
                <th>nombre</th>
                <th>apellido</th>
                <th>correo</th>
                <th>literatura</th>
                </thead>
                <tbody>
                    <% while (itlista.hasNext()) {
                    Inscripcion ins = itlista.next();%>
                    <tr>
                        <td><%= ins.getRut()%></td>
                        <td><%= ins.getNombre()%></td>
                        <td><%= ins.getApellido()%></td>
                        <td><%= ins.getCorreo()%></td>
                        <td><%= ins.getLiteratura()%></td>
                        <td> <input type="radio" name="seleccion" value="<%= ins.getRut()%>"> </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
                 <br>
            <button type="submit" name="accion" value="inscribir" class="btn btn-success">Unirse al Club</button>
            <button type="submit" name="accion" value="editar" class="btn btn-info">Editar datos</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-danger">Eliminar datos</button>
        </form>
    </body>
</html>
