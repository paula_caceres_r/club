<%-- 
    Document   : index
    Created on : 05-10-2021, 20:46:25
    Author     : paula
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <br>
        <h2 style="background-color:Orange;">Únete a nuestro Club de Literatura</h2>
        <br>
        <img src="file.png" class="img-circle" alt="Cinque Terre">
        <br>
        <h2>Ingresa tus datos</h2>
        <br>
        <form class="form-horizontal" action="InscripcionController" method="POST" >
            <div class="form-group">
                <label class="control-label col-sm-2" for="rut">Rut:</label >
                <div class="col-sm-10">
                    <input type="text" name="rut" class="form-control" id="rut" placeholder="Ingrese su rut">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="control-label col-sm-2" for="nombre">Nombre:</label>
                <div class="col-sm-10">
                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese su nombre">
                </div>
                <br>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="apellido">Apellido:</label>
                <div class="col-sm-10">
                    <input type="text" name="apellido" class="form-control" id="apellido" placeholder="Ingrese su apellido">
                </div>
                <br>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="correo">Correo electrónico:</label>
                <div class="col-sm-10">
                    <input type="email" name="correo" class="form-control" id="correo" placeholder="Correo electrónico">
                </div>
                <br>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="literatura">Literatura favorita:</label>
                <div class="col-sm-10">
                    <input type="text" name="literatura" class="form-control" id="literatura" placeholder="Ingrese literatura favorita">
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="accion" value="grabar" class="btn btn-success">Grabar</button>
                </div>
            </div>
        </form>
    </body>

</html>
