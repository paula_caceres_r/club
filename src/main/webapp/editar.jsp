<%-- 
    Document   : index
    Created on : 05-10-2021, 20:46:25
    Author     : paula
--%>

<%@page import="com.mycompany.club.entity.Inscripcion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
   Inscripcion inscripcion = (Inscripcion) request.getAttribute("inscripcion");

  
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <script src="js/bootstrap.js" type="text/javascript"></script>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <br>
        <h1 style="background-color:Orange;">Editar datos</h1>
         <br>
                <form class="form-horizontal" action="InscripcionController" method="POST">
              <div class="form-group">
                <label class="control-label col-sm-2" for="rut">Rut:</label>
                <div class="col-sm-10">
                    <input type="text" name="rut" class="form-control" id="rut" value="<%= inscripcion.getRut()%>" placeholder="Ingrese su rut">
                </div>
                 <br>
            </div>
               <div class="form-group">
                <label class="control-label col-sm-2" for="nombre">Nombre:</label>
                <div class="col-sm-10">
                    <input type="text" name="nombre" class="form-control" id="nombre" value="<%= inscripcion.getNombre()%>" placeholder="Ingrese su nombre">
                </div>
                 <br>
            </div>
                <div class="form-group">
                <label class="control-label col-sm-2" for="apellido">Apellido:</label>
                <div class="col-sm-10">
                    <input type="text" name="apellido" class="form-control" id="apellido" value="<%= inscripcion.getApellido()%>" placeholder="Ingrese su apellido">
                </div>
                 <br>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="correo">Correo electrónico:</label>
                <div class="col-sm-10">
                    <input type="email" name="correo" class="form-control" id="correo" value="<%= inscripcion.getCorreo()%>" placeholder="Correo electrónico">
                </div>
                 <br>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="literatura">Literatura favorita:</label>
                <div class="col-sm-10">
                    <input type="text" name="literatura" class="form-control" id="literatura" value="<%= inscripcion.getLiteratura()%>" placeholder="Ingrese literatura favorita">
                </div>
                 <br>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="accion" value="grabareditar" class="btn btn-success">Guardar cambios</button>
                </div>
            </div>
        </form>
    </body>
</html>
