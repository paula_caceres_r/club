/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.club.controller;

import com.mycompany.club.dao.InscripcionJpaController1;
import com.mycompany.club.dao.exceptions.NonexistentEntityException;
import com.mycompany.club.entity.Inscripcion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author paula
 */
@WebServlet(name = "InscripcionController", urlPatterns = {"/InscripcionController"})
public class InscripcionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InscripcionController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InscripcionController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        InscripcionJpaController1 dao = new InscripcionJpaController1();
        String accion = request.getParameter("accion");

        if (accion.equals("inscribir")) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

        if (accion.equals("eliminar")) {
            try {
                String seleccion = request.getParameter("seleccion");

                dao.destroy(seleccion);
            } catch (Exception ex) {
                Logger.getLogger(InscripcionController.class.getName()).log(Level.SEVERE, null, ex);
            }

            List<Inscripcion> listaInscripcion;
            listaInscripcion = dao.findInscripcionEntities();

            request.setAttribute("lista", listaInscripcion);

            request.getRequestDispatcher("listar.jsp").forward(request, response);

        }

        if (accion.equals("editar")) {
            

            try {
                String seleccion = request.getParameter("seleccion");

                Inscripcion inst = dao.findInscripcion(seleccion);
                request.setAttribute("inscripcion", inst);

            } catch (Exception ex) {
                Logger.getLogger(InscripcionController.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }

            request.getRequestDispatcher("editar.jsp").forward(request, response);

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String botonseleccionado = request.getParameter("accion");

        InscripcionJpaController1 dao = new InscripcionJpaController1();

        try {
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String correo = request.getParameter("correo");
            String literatura = request.getParameter("literatura");
            Inscripcion inscripcion = new Inscripcion();
            inscripcion.setRut(rut);
            inscripcion.setNombre(nombre);
            inscripcion.setApellido(apellido);
            inscripcion.setCorreo(correo);
            inscripcion.setLiteratura(literatura);

            if (botonseleccionado.equals("grabar")) {

                dao.create(inscripcion);
            } else {
                dao.edit(inscripcion);
            }

        } catch (Exception ex) {
            Logger.getLogger(InscripcionController.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<Inscripcion> listaInscripcion;
        listaInscripcion = dao.findInscripcionEntities();

        request.setAttribute("lista", listaInscripcion);

        request.getRequestDispatcher("listar.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
